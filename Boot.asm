;**************************************************;

;		Author: Blue Pyro (real name: Paul Abrams)
;		Name: BPOS (Blue Pyro OS)
;		Version: A01

;		Notes:
;		A first attempt at a 16 bit full blown OS.
;		Hopeing to create a custom filesystem mostly
;		for educational purposes.  The goal is to
;		make this OS capable of producing programs
;		which may run directly inside it.  Secondary
;		goal will be to make the OS itself
;		programmable/modifiable from inside the OS.
;		Worst case i'll have to settle with some
;		custom scripting language...  (cringe)
;**************************************************;

;TODO: setup some basic command line functions and call them in BPOS

;----Include macros.----;
%include "include/bios_macros.asm"
;----END----;



;[org 0x7c00]

jmp bootloaderMain

%include "include/bios_print.asm"
%include "include/bios_print_hex.asm"
%include "include/read_disk.asm"



bootloaderMain:
;mov bp, 0x8000 ; set the stack safely away from us
;mov sp, bp

; mov ax, 07C0h   ; Set 'ax' equal to the location of this bootloader divided by 16
; add ax, 20h     ; Skip over the size of the bootloader divided by 16
; mov ax, (0x7c00+512)/16
; mov ss, ax      ; Set 'ss' to this location (the beginning of our stack region)
; mov ds, ax      ; Set 'ds' to the this location
; mov ax, 0
; ;mov ax, stackBeginning
; ;mov ax, 4096
; ;mov bp, ax
; ;mov ax, stackEnding
; mov ax, 4096
; mov sp, ax

 ; ;----------------------------------------------------
 ; ; code located at 0000:7C00, adjust segment registers
 ; ;----------------------------------------------------

      ; cli                       ; disable interrupts
      ; mov     ax, 0x07C0                ; setup registers to point to our segment
      ; mov     ds, ax
      ; mov     es, ax
      ; mov     fs, ax
      ; mov     gs, ax

 ; ;----------------------------------------------------
 ; ; create stack
 ; ;----------------------------------------------------

      ; mov     ax, (0x7c00+512+40960+4096)/16                ; set the stack
      ; mov     ss, ax
      ; mov     bp, 4096
	  ; mov     sp, bp
      ; sti                       ; restore interrupts
	
	cli
	mov ax, 0x0000
	mov ss, ax
	mov sp, 0xffff
	sti
	
	mov ax, 0x07c0
	mov ds, ax
	mov es, ax

mov bx, BPOS ;WAS: es:bx = 0x0000:0x9000 = 0x09000.  Now it seems to point directly to the OS(in ram?).
mov dh, 60 ; Read 60 sectors.  Each sector is exactly 512 bytes long.
; the bios sets 'dl' for our boot disk number
; if you have trouble, use the '-fda' flag: 'qemu -fda file.bin'
call disk_load

; mov dx, [0x9000] ; retrieve the first loaded word, 0xdada
; call print_hex
; call print_nl

; mov dx, [0x9000 + 512] ; first word from second loaded sector, 0xface
; call print_hex
; call print_nl

BIOS_INIT_VIDEO(01h)
mov bx, strSecondStageSuccess
call print
call print_nl

call print_nl
call pause
jmp BPOS
;--END--;



;--------;
;--DATA--;
strSecondStageSuccess: db "Successfully loaded OS.  Booting...",0
;--------;



; Magic number
times 510 - ($-$$) db 0
dw 0xaa55
bootEnd:

;times 256 dw 0xdada ; sector 2 = 512 bytes
;times 256 dw 0xface ; sector 3 = 512 bytes
;%include "BPOS.asm"



%include "BPOS.asm"
BPOSEnding:

stackBeginning:
times 2048 dw 0x1111
stackEnding:

times 2048 dw 0x1



; boot sector = sector 1 of cyl 0 of head 0 of hdd 0
; from now on = sector 2 ...
times 256*1250 dw 0x1000 ; sector 4 to n = 512 bytes each; Total = 512*n