;**************************************************;
;		BPOS is our main operating system.
;**************************************************;
%define NL 0x0d, 0x0a ;this is \r and \n (use this to newline strings).

[bits 16]



BPOS:
	
	mov bx, msgOSWelcome
	call print
	call print_nl
	
	call print_nl
	call print_nl
	
	call pause
	BIOS_INIT_VIDEO_LOSELESS(03h)
	.cmdLoop:
	call cmd
	jmp .cmdLoop
	

jmp $  ;safty catch
msgOSWelcome: db "Entering OS.  Starting command prompt.",0

%include "include/bios_cmd.asm"

times 40960 - ($-BPOS) db 0