;**************************************************;
;		Command line interface/functions.
;**************************************************;



%define TEXT_BUFFER_LENGTH 100
textBuffer: TIMES TEXT_BUFFER_LENGTH db 0  ;used to store a string.
.count dw 0

msgWelcomeToCmd: db "Welcome to command prompt.", NL, "Type help for list of commands.",0
cmd:  ;call this to begin command line.
	mov bx, msgWelcomeToCmd
	call print
	call print_nl
	call typeToBuffer
	
	mov ax, .cmdHelp
	mov bx, textBuffer
	call cmpStrings
	cmp ax, 1
	je help
	
	mov ax, .cmdPause
	mov bx, textBuffer
	call cmpStrings
	cmp ax, 1
	je pause
	
	mov ax, .cmdCls
	mov bx, textBuffer
	call cmpStrings
	cmp ax, 1
	je cls
	
	
	.end:
	ret
.cmdHelp: db "help", 0
.cmdPause: db "pause", 0
.cmdCls: db "cls", 0



;****************************************************************************
;
; cmpStrings - Compare two strings.
;
; INPUT:        ax = first label, bx = second label
; OUTPUT:       ax = return true/false(1/0)
; DESTROYED:    all
;****************************************************************************
cmpStrings:
	pusha
	call print_nl
	mov cx, bx
	xor bx, bx
	
	.loop:
	add bx, ax
	mov dl, [bx]
	sub bx, ax
	
	add bx, cx
	mov dh, [bx]
	sub bx, cx
	
	cmp dl, dh
	jne .endFalse
	cmp dh, 0
	je .endTrue
	
	inc bx
	jmp .loop
	

	.endTrue:
	popa
	mov ax, 1
	ret
	
	.endFalse:
	popa
	xor ax, ax
	ret



typeToBuffer:
	pusha
	xor cx, cx  ;counter
	.loop:
	;if we are at end of buffer then display warning message and end here.
	cmp cx, TEXT_BUFFER_LENGTH-1
	je .outOfBounds
	
	;grab typed character till "enter" is pressed.
	mov ah, 0h
	int 16h
	
	;if "enter" is pressed then end.
	cmp al, 0x0d
	je .cleanUp
	
	mov bx, cx ;give bx the counter as only bx can be used with address.
	mov [textBuffer+bx], al  ;return typed char into textBuffer.
	add bx, textBuffer  ;give address of current character typed into buffer.
	
	call printChar
	inc cx
	jmp .loop
	
	.cleanUp:
	mov bx, cx
	.cleanUpLoop:
	cmp bx, TEXT_BUFFER_LENGTH-1
	jge .end
	mov byte[textBuffer+bx], 0
	inc bx
	jmp .cleanUpLoop

	.end:
	popa
	ret

	.outOfBounds:
	call print_nl
	mov bx, .msgTextTooLong
	call print
	call print_nl
	jmp .end
.msgTextTooLong: db "Text inside buffer is too long!  Aborting!", 0



help:
	mov bx, .msg
	call print
	call print_nl
	call typeToBuffer
	
	mov ax, cmd.cmdPause
	mov bx, textBuffer
	call cmpStrings
	cmp ax, 1
	je pause.help
	
	mov ax, cmd.cmdCls
	mov bx, textBuffer
	call cmpStrings
	cmp ax, 1
	je cls.help
	
	ret
.msg: db "This is the help command.", NL
db "pause", NL
db "cls", NL
db "Type the name of a command for more information.", NL, NL, NL, NL, 0



pause:  ;Pauses window till key is pressed.
	mov bx, .msgPause
	call print
	call print_nl
	push ax
	mov ah, 0h
	int 16h
	pop ax
	ret
.msgPause: db "Press any key to continue...",0
.help:
	mov bx, .helpData
	call print
	ret
.helpData: db "pauses till key press.", NL, NL,0

cls:  ;Clears the screen.  TODO: fix
	push ax
	xor ax, ax
	.loop:
	call print_nl
	call print_nl
	call print_nl
	inc ax
	cmp ax, 10+1
	jl .loop
	pop ax
	ret
.help:
	mov bx, .helpData
	call print
	ret
.helpData: db "Clears the screen.", NL, NL, 0