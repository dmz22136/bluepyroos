;****************************************************************************
;
; print - Print null terminated string.
;
; INPUT:        bx = label to print
; OUTPUT:       none
; DESTROYED:    none
;****************************************************************************

print:
    pusha

; keep this in mind:
; while (string[i] != 0) { print string[i]; i++ }

; the comparison for string end (null byte)
.start:
    mov al, [bx] ; 'bx' is the base address for the string
    cmp al, 0 
    je .done

    ; the part where we print with the BIOS help
    mov ah, 0x0e
	mov cx, bx
	mov bl, 2 ;white foreground
    int 0x10 ; 'al' already contains the char
	mov bx, cx

    ; increment pointer and do next loop
    inc bx
    jmp .start

.done:
    popa
    ret



print_nl:
    pusha
    
    mov ah, 0x0e
    mov al, 0x0a ; newline char
    int 0x10
    mov al, 0x0d ; carriage return
    int 0x10
    
    popa
    ret



printChar:
	pusha
	
	mov ah, 0x0e
	mov al, [bx]
	int 0x10
	
	popa
	ret