;**************************************************;
;		Bios macros.
;**************************************************;



%define VGA_MODE 13h



;call video services
%macro BIOS_VIDEO_SERVICES 0
	int 10h
%endmacro

;init video(video mode)
%macro BIOS_INIT_VIDEO 1
	push ax
	xor ah, ah
	mov al, %1
	BIOS_VIDEO_SERVICES
	pop ax
%endmacro

;init video(video mode) without clearing screen
%macro BIOS_INIT_VIDEO_LOSELESS 1
	push ax
	xor ah, ah
	mov al, %1+128
	BIOS_VIDEO_SERVICES
	pop ax
%endmacro

%macro BIOS_INIT_VIDEO_VGA 0
	BIOS_INIT_VIDEO(VGA_MODE)
%endmacro